REQUIREMENTS
==================
* npm
* node
* mysql-server

INSTALL AND RUN BACKEND
=====================
* cd to root of project
* run `npm install`
* create mysql database
* edit backed/config/config.json
* cd to backend folder
* run migrations ../node_modules/sequelize-cli/bin/sequelize db:migrate
* run node ./www
* visit localhost:8080 in browser

RUN WORKER
=============================
* cd to worker folder
* edit config/config.json
* run node ./app

If you want to run multiple workers on one pc, you can use additional command line arguments to modify config on the fly: id, secret, host, port.
for example: node ./app --id=1 --secret=secret --host=localhost --port=8080

If you will run some difficult code with lot of dependencies in workers, please check
the worker vm sandbox (worker/components/vm.js) and maybe modify it to omit execution errors.


RUN CLIENT
===========================
* cd to client folder
* edit config/config.json
* implement your apps using client api