var st  = require('stun'),
    client,
    host = 'stun.l.google.com',
    port = 19302;


var stun = {
    connect: function(success, failure){
        client = st.connect(port, host);
        client.on('error', failure);

        client.once('response', function(packet){
            var address;

            if (packet.attrs[st.attribute.XOR_MAPPED_ADDRESS]) {
                address = packet.attrs[st.attribute.XOR_MAPPED_ADDRESS];
            } else {
                address = packet.attrs[st.attribute.MAPPED_ADDRESS];
            }

            if(address) {
                success(client._socket, address);
            }
        });

        client.request(function(){
            console.log('Sending STUN packet');
        });
    }
};

module.exports = stun;
