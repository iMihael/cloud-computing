var NodeVM = require('vm2').NodeVM,
    vm = require('vm'),
    fs = require('fs'),
    file = require('./file'),
    randomstring = require("randomstring");

module.exports = {
    executeCodeVM2: function(inputFileVar, outputFileVar, code, inputFileFd, success, failure){

        var outputFile = {};
        var sandbox = {};

        sandbox[inputFileVar] = inputFileFd;

        outputFile.name = randomstring.generate({
                length: 8,
                charset: 'alphabetic'
        });

        console.log('outputfile name', outputFile.name);

        outputFile.path = __dirname + '/../storage/' + outputFile.name;
        outputFile.fd = fs.openSync(outputFile.path, "w+");
        sandbox[outputFileVar] = outputFile.fd;


        var vm = new NodeVM({
            require: true,
            requireExternal: true,
            requireNative: [],
            useStrict: false,
            sandbox: sandbox
        });

        try {
            vm.run(code);
        } catch (e) {
            failure(JSON.stringify(e));
        }

        var stats = fs.statSync(outputFile.path);
        outputFile.size = stats.size;

        file.hashFile(outputFile.path, function(hash){
            outputFile.hash = hash;
            success(outputFile);
        });
    },
    executeCodeVM: function(inputFileVar, outputFileVar, code, inputFile, success, failure){

        var outputFile = {};
        var sandbox = {
            require: require,
            Buffer: Buffer
        };

        if(inputFileVar) {
            sandbox[inputFileVar] = inputFile.path;
        }

        outputFile.name = randomstring.generate({
            length: 8,
            charset: 'alphabetic'
        });

        console.log('outputfile name', outputFile.name);

        outputFile.path = __dirname + '/../storage/' + outputFile.name;
        outputFile.fd = fs.openSync(outputFile.path, "w+");
        sandbox[outputFileVar] = outputFile.path;

        var context = new vm.createContext(sandbox);
        var script = new vm.Script(code);

        try {
            script.runInContext(context);
        } catch(e) {
            console.log(typeof(e), e);
            failure(JSON.stringify(e));
            return;
        }

        if(inputFileVar) {
            inputFile.path = sandbox[inputFileVar];
        }

        outputFile.path = sandbox[outputFileVar];
        outputFile.fd = fs.openSync(outputFile.path, "r+");

        var stats = fs.statSync(outputFile.path);
        outputFile.size = stats.size;

        file.hashFile(outputFile.path, function(hash){
            outputFile.hash = hash;
            success(outputFile);
        });
    }
};