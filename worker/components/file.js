var fs = require('fs'),
    crypto = require('crypto');

module.exports = {
    hashFile: function(file, success) {
        var hash = crypto.createHash('sha1'),
            stream = fs.createReadStream(file);

        stream.on('data', function(data){
            hash.update(data, 'utf8');
        });

        stream.on('end', function(){
            stream.close();
            success(hash.digest('hex'));
        });

    }
};