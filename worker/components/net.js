var ip = require('ip'),
    net = require('net'),
    portfinder = require('portfinder'),
    tcpSocket,
    localServer;

// использование Math.round() даст неравномерное распределение!
var getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};


var network = {
    getLocalAddress: function(){
        return ip.address();
    },
    stopLocalServer: function(){
        localServer.close();
    },
    sendData: function(data){
        if(tcpSocket) {
            tcpSocket.write(data);
        }
    },
    createLocalServer: function(reader, success, failure) {

        portfinder.basePort = getRandomInt(9000, 63000);

        portfinder.getPort(function(err, port){

            localServer = net.createServer(function(socket){
                tcpSocket = socket;
                tcpSocket.on('data', reader);
                tcpSocket.on('error', function(error){
                    failure(error);
                });
            });

            localServer.listen(port, function (err) {
                if(!err) {
                    success(port);
                } else {
                    failure(err);
                }
            });

            localServer.on('error', function(error){
                //console.log(error);
                failure(error);
            });
        });
    }
};

module.exports = network;