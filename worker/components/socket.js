var config = require('../config/config'),
    fs = require('fs'),
    vm = require('./vm'),
    net = require('./net'),
    file = require('./file'),
    stun = require('./stun'),
    randomstring = require("randomstring"),
    taskData = null,
    chunkSize = 512,
    clientAuth = false,
    clientStunAddress = {},
    outputFile = {},
    inputFile = {},
    sock,
    tcpPacket = new Buffer(0);


var killTimeout = 5000;
var killId;
var scheduleKill = function(){
    // if(killId) {
    //     clearTimeout(killId);
    // }
    //
    // killId = setTimeout(socket.abortTask, killTimeout);
};




var socket = {
    methods: {
        ping: function(writer) {
            socket.sendJson(writer, {
                method: 'ping',
                data: 'pong'
            });
        },
        inputFile: function(writer, data){
            if(data.hasOwnProperty('size') && data.hasOwnProperty('hash')) {
                inputFile = {};
                inputFile.size = data.size;
                inputFile.hash = data.hash;
                inputFile.receivedBytes = 0;

                inputFile.name = randomstring.generate({
                    length: 16,
                    charset: 'alphabetic'
                });

                console.log('file name', inputFile.name);

                inputFile.path = __dirname + '/../storage/' + inputFile.name;
                inputFile.fd = fs.openSync(inputFile.path, 'w+');

                socket.sendJson(writer, {
                    method: 'inputFile',
                    data: {
                        taskId: data.taskId,
                        status: 'ready'
                    }
                });
            }
        },
        auth: function(writer, data) {
            if(data.hasOwnProperty('taskId') && data.taskId == taskData.id) {
                clientAuth = true;
                socket.sendJson(writer, {
                    method: 'auth',
                    data: {
                        taskId: data.taskId,
                        status: 'success'
                    }
                });
            } else {
                socket.sendJson(writer, {
                    method: 'auth',
                    data: {
                        taskId: data.taskId,
                        status: 'failed'
                    }
                });
            }
        },
        inputCode: function(writer, data) {
            if(data.hasOwnProperty('inputCode')) {
                var func = data.inputCode;
                var match1 = func.match(/function \(([a-zA-Z_$][a-zA-Z0-9]*),\s*([a-zA-Z_$][a-zA-Z0-9]*)\){([\s\S]*)}/m);
                var match2 = func.match(/function \(([a-zA-Z_$][a-zA-Z0-9]*)\){([\s\S]*)}/m);

                var failure = function(err){
                    //send error
                    socket.taskFailure(err);
                    socket.abortTask();
                };

                if(killId) {
                    clearTimeout(killId);
                }

                if(match1 && match1.length >= 4) {
                    vm.executeCodeVM(match1[1], match1[2], match1[3], inputFile, function (oFile) {
                        outputFile = oFile;

                        if(taskData) {
                            socket.sendJson(writer, {
                                method: 'outputFile',
                                data: {
                                    taskId: taskData.id,
                                    size: outputFile.size,
                                    hash: outputFile.hash
                                }
                            });
                        } else {
                            socket.abortTask();
                        }
                    }, failure);

                } else if(match2 && match2.length >= 3) {
                    vm.executeCodeVM(false, match2[1], match2[2], false, function (oFile) {
                        outputFile = oFile;

                        socket.sendJson(writer, {
                            method: 'outputFile',
                            data: {
                                taskId: taskData.id,
                                size: outputFile.size,
                                hash: outputFile.hash
                            }
                        });
                    }, failure);
                }

            }
        },
        outputFile: function(writer){
            outputFile.bytesSent = 0;

            do {
                var buf = new Buffer(chunkSize);
                var read = fs.readSync(outputFile.fd, buf, 0, chunkSize, outputFile.bytesSent);
                if(read < chunkSize) {
                    buf = buf.slice(0, read);
                }

                outputFile.bytesSent += read;
                writer(buf);
            } while(outputFile.bytesSent < outputFile.size);
        },
        fileReceived: function(){
            try {
                fs.unlinkSync(inputFile.path);
                fs.unlinkSync(outputFile.path);
            } catch (e) {
                console.error(e);
            }
            
            outputFile = {};
            inputFile = {};
            socket.abortTask();
        }
    },
    sendJson: function(writer, data){
        writer(new Buffer(JSON.stringify(data)));
    },
    abortTask: function() {
        net.stopLocalServer();
        taskData = null;
        clientAuth = false;
        sock.emit('updateStatus', 'ready');

        if(killId) {
            clearTimeout(killId);
        }
    },
    dataReader: function(bytes, writer){

        scheduleKill();

        var parsePacket = function(bytes){
            var call = JSON.parse(bytes.toString());
            if (call.hasOwnProperty('method') && call.hasOwnProperty('data') && socket.methods.hasOwnProperty(call.method)) {
                if ((!clientAuth && (call.method == 'ping' || call.method == 'auth')) || clientAuth) {
                    socket.methods[call.method](writer, call.data);
                }
            }
        };

        if(!inputFile.hasOwnProperty('size') || inputFile.receivedBytes >= inputFile.size) {
            try {
                parsePacket(bytes);
            } catch (e) {
                if(tcpPacket.length == 0) {
                    tcpPacket = bytes;
                } else {
                    tcpPacket = Buffer.concat([tcpPacket, bytes]);

                    try {
                        parsePacket(tcpPacket);
                        tcpPacket = new Buffer(0);
                    } catch (e) {
                        //wait for other packet
                    }

                }
            }

        } else {

            fs.writeSync(inputFile.fd, bytes, 0, bytes.length);
            inputFile.receivedBytes += bytes.length;

            if(inputFile.receivedBytes >= inputFile.size) {

                file.hashFile(inputFile.path, function(hash){

                    console.log('file received', hash);

                    if(hash == inputFile.hash) {
                        console.log('hash is right!');
                        socket.sendJson(writer, {
                            method: 'fileReceived',
                            data: {
                                taskId: taskData.id
                            }
                        });
                    } else {
                        console.error('wrong hash!');
                        //send error
                        socket.taskFailure('wrong input file hash');
                        socket.abortTask();
                    }
                });

            }
        }
    },
    addTask: function (data) {
        if(!taskData) {
            taskData = data;
            clientStunAddress = data.stunAddress;

            stun.connect(function(udpSocket, stunAddress){

                net.createLocalServer(function(bytes){
                    //LOCAL SERVER READER
                    socket.dataReader(bytes, function(data, success, error){
                        net.sendData(data, success, error);
                    });
                    

                }, function (localPort) {

                    udpSocket.on('message', function(bytes) {
                        socket.dataReader(bytes, function (data, success, error) {
                            if (udpSocket && clientStunAddress.hasOwnProperty('port')) {
                                udpSocket.send(data, 0, data.length, clientStunAddress.port, clientStunAddress.address);
                                success();
                            } else {
                                error();
                            }
                        });
                    });

                    scheduleKill();

                    sock.emit('addTask', {
                        boolean: true,
                        status: 'success',
                        data: {
                            localAddress: {
                                address: net.getLocalAddress(),
                                port: localPort
                            },
                            stunAddress: stunAddress
                        }
                    });

                }, function () {

                    sock.emit('addTask', {
                        boolean: false,
                        status: 'error',
                        reason: 'Worker can not start local server'
                    });
                });

            }, function () {

                sock.emit('addTask', {
                    boolean: false,
                    status: 'error',
                    reason: 'Can not connect to stun server'
                });
            });
        }
    },
    taskFailure: function(reason) {
        sock.emit('taskFailure', {
            taskId: taskData.id,
            reason: reason
        });
    },
    init: function(argv, logged, failure){

        for(var i=0;i<argv.length;i++) {
            var matches = argv[i].match(/^--([a-zA-Z0-9]+)=([a-zA-Z0-9.\-]+)$/);
            if(matches) {
                config[matches[1]] = matches[2];
            }
        }

        var address = 'http://' + config.host + ':' + config.port;

        sock = require('socket.io-client').connect(
            address + "?type=worker&id=" + config.id + "&secret=" + config.secret +
            "&localAddress=" + net.getLocalAddress()
        );

        sock.on('err', function(error){
            console.error(error);
            failure(error);
        });

        sock.on('error', function(error) {
            console.error(error);
        });

        sock.on('disconnect', function(){
            failure('socket disconnected');
        });

        sock.on('logged', function(){

            console.log('Connected to backend.');

            sock.on('addTask', socket.addTask);
            sock.on('abortTask', socket.abortTask);

            logged();
        });

        return sock;
    }
};

module.exports = socket;
