var Client = require('../models/client'),
    randomstring = require("randomstring");

module.exports = {
    list: function(req, res) {
        Client.byUserId(req.user.id, function(clients){
            res.render('client/list', {
                title: 'Client - List',
                clients: clients
            });
        });
    },
    add: function(req, res) {
        res.render('client/add', {
            title: 'Client - Add',
            model: req.flash('model') ? req.flash('model') : {},
            errors: req.flash('errors')
        });
    },
    doAdd: function(req, res) {
        var w = Client.build(req.body);
        w.userId = req.user.id;
        w.secret = randomstring.generate({
            length: 32,
            charset: 'alphabetic'
        });

        w.validate().then(function(errors){
            if(!errors) {
                w.save().then(function(){
                    res.redirect('list');
                });
            } else {
                req.flash('errors', errors);
                req.flash('model', w);
                res.redirect('add');
            }
        });
    },
    edit: function(req, res) {
        Client.byIdAndUserId(req.params.id, req.user.id, function(model) {
            res.render('client/edit', {
                title: 'Client - Edit',
                errors: req.flash('errors'),
                model: model
            });
        }, function(){
            res.end();
        });
    },
    doEdit: function(req, res) {
        Client.byIdAndUserId(req.params.id, req.user.id, function(model) {

            model.set(req.body);
            model.validate().then(function (errors) {
                if(errors) {
                    req.flash('errors', errors);
                    res.redirect(req.get('referer'));
                } else {
                    model.save();
                    res.redirect('../list');
                }
            });

        }, function(){
            res.end();
        });
    },
    delete: function(req, res) {
        Client.byIdAndUserId(req.params.id, req.user.id, function(model){
            model.destroy();
            res.redirect('../list');
        }, function(){
            res.end();
        })
    },
    updateSecret: function(req, res) {
        Client.byIdAndUserId(req.params.id, req.user.id, function(model){
            model.secret = randomstring.generate({
                length: 32,
                charset: 'alphabetic'
            });
            model.save();
            res.redirect('../list');
        }, function(){
            res.end();
        });
    }
};