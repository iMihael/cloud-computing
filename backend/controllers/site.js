var User = require('../models/user'),
    passwordHash = require('password-hash');

module.exports = {
    index: function(req, res) {

        res.render(req.user ? 'site/indexPrivate' : 'site/index', {
            title: 'Cloud Computing',
            user: req.user
        });
    },
    login: function(req, res) {
        res.render('site/login', {
            title: 'Login',
            body: {},
            error: req.flash('error'),
            user: req.user
        });
    },
    logout: function (req, res) {
        req.logout();
        res.redirect('/');
    },
    register: function(req, res){
        res.render('site/register', {
            title: 'Register',
            errors: req.flash('errors'),
            body: req.flash('body') ? req.flash('body') : {}
        });
    },
    doRegister: function(req, res) {
        req.checkBody('email', 'Email is required and must have right format.').notEmpty().isEmail();
        req.checkBody('firstName', 'First Name is required.').notEmpty();
        req.checkBody('lastName', 'Last Name is required.').notEmpty();
        req.checkBody('password', 'Password is required.').notEmpty();
        req.checkBody('confirmPassword', 'Password must be confirmed exactly.').notEmpty().equals(req.body.password);

        var errors = req.validationErrors();

        if (errors) {
            req.flash('errors', errors);
            req.flash('body', req.body);
            res.redirect('/register');
        } else {
            User.findOne({
                where: {
                    email: req.body.email
                }
            }).then(function (user) {
                if (user) {
                    errors = [{
                        msg: 'Email already exists in system.'
                    }];

                    req.flash('errors', errors);
                    req.flash('body', req.body);
                    res.redirect('/register');
                } else {
                    req.body.password = passwordHash.generate(req.body.password, {
                        algorithm: 'sha256'
                    });

                    User.create(req.body).then(function(user) {
                        req.login(user, function(){
                            res.redirect('/');
                        });
                    });

                }
            });
        }
    }
};