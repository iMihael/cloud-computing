var express = require('express'),
    router = express.Router(),
    passport = require('../components/auth'),
    site = require('../controllers/site');

var isGuest = function(res, req, next){
    if(req.user) {
        res.redirect('/');
    } else {
        next();
    }
};

router.get('/', site.index);

router.get('/logout', site.logout);

router.get('/login', isGuest, site.login);

router.post('/login',
    passport.authenticate('local', { successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: 'Invalid username or password.'
    })
);

router.get('/register', isGuest, site.register);
router.post('/register', isGuest, site.doRegister);

module.exports = router;