var router = require('express').Router(),
    worker = require('../controllers/worker'),
    client = require('../controllers/client');

// worker routes
router.get('/worker/list', worker.list);

router.get('/worker/add', worker.add);
router.post('/worker/add', worker.doAdd);

router.get('/worker/edit/:id', worker.edit);
router.post('/worker/edit/:id', worker.doEdit);

router.get('/worker/secret/:id', worker.updateSecret);
router.get('/worker/delete/:id', worker.delete);


// client routes
router.get('/client/list', client.list);

router.get('/client/add', client.add);
router.post('/client/add', client.doAdd);

router.get('/client/edit/:id', client.edit);
router.post('/client/edit/:id', client.doEdit);

router.get('/client/secret/:id', client.updateSecret);
router.get('/client/delete/:id', client.delete);

module.exports = router;