var session = require('express-session');

var MySQLStore = require('express-mysql-session');
var env       = process.env.NODE_ENV || 'development';
var conf = require('../config/config.json')[env];

var options = {
    host: conf.host,
    port: 3306,
    user: conf.username,
    password: conf.password,
    database: conf.database,
    createDatabaseTable: true
};

var sessionStore = new MySQLStore(options);

module.exports = session({
    store: sessionStore,
    resave: true,
    cookie: {
        maxAge: 2592000
    },
    saveUninitialized: true,
    secret: 'dqwdqwdcxzczxcdwqdqwd'
});