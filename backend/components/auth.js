var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var passwordHash = require('password-hash');
var User = require('../models/user');

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(username, password, done) {
        User.findOne({
            where: {
                email: username
            }
        }).then(function(user){
            if (!user) {
                return done(null, false, { message: 'Incorrect email or password.' });
            }
            if (!passwordHash.verify(password, user.password)) {
                return done(null, false, { message: 'Incorrect email or password.' });
            }

            return done(null, user);
        });
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {

    User.findOne({
        where: {
            id: id
        }
    }).then(function(user){
        if(user) {
            done(null, user);
        } else {
            done('User not found.');
        }
    });
});

module.exports = passport;