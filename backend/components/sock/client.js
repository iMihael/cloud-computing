var Worker = require('../../models/worker'),
    socket = require('../sock'),
    randomstring = require("randomstring");

var client = {
    init: function(){
        socket = require('../sock');
    },
    addListeners: function(sock){
        sock.on('getFreeWorkersCount', client.getFreeWorkersCount);
        sock.on('addTask', client.addTask);
        sock.on('finishTask', client.finishTask);
    },
    finishTask: function(data){
        console.log(data);
        for(var i=0;i<this.workers.length;i++) {
            if(this.workers[i].hasOwnProperty('taskId') && this.workers[i].taskId == data.taskId) {
                this.workers[i].model.successTasks++;
                this.workers[i].model.workTime += Date.now() - this.workers[i].taskStart;
                this.workers[i].model.save();
                this.workers.splice(i, 1);
                return;
            }
        }
    },
    getFreeWorkersCount: function(){
        //TODO: implement some limits based on client rate
        var sock = this;
        Worker.readyWorkersCount(function(count){
            sock.emit('getFreeWorkersCount', count[0]['count']);
        });

    },
    addTask: function(taskData){
        var taskId = taskData.id;
        //find free worker socket
        var workerSocket = socket.getFreeWorker();
        var sock = this;
        if(workerSocket) {

            workerSocket.model.status = 'working';
            workerSocket.model.save();

            workerSocket.once('addTask', function(workerData){
                if(workerData.hasOwnProperty('boolean') && workerData.boolean) {
                    if(!sock.hasOwnProperty('workers')) {
                        sock.workers = [];
                    }

                    //assign worker with client
                    workerSocket.tempId = randomstring.generate({
                        length: 16,
                        charset: 'alphabetic'
                    });

                    workerSocket.taskId = taskId;
                    workerSocket.taskStart = Date.now();
                    sock.workers.push(workerSocket);
                    workerData.workerTempId = workerSocket.tempId;
                    sock.emit('addTask' + taskId, workerData);
                } else {
                    workerSocket.model.status = 'ready';
                    workerSocket.model.save();

                    sock.emit('addTask' + taskId, data);
                }
            });

            workerSocket.emit('addTask', taskData);
        } else {
            this.emit('addTask', {
                boolean: false,
                status: 'error',
                reason: 'No free workers'
            });
        }
    }
};


module.exports = client;