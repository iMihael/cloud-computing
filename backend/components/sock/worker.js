var socket = require('../sock');

var worker = {
    init: function(){
        socket = require('../sock');
    },
    addListeners: function(sock){
        sock.on('updateStatus', worker.updateStatus);
        sock.on('taskFailure', worker.taskFailure);
    },
    updateStatus: function(data){
        this.model.status = data;
        this.model.save();
    },
    taskFailure: function(data) {
        console.log(data);

        var sockets = socket.getSockets();
        for(var i=0;i<sockets.length;i++) {
            if(sockets[i].modelType == 'client') {
                for(var j=0;j<sockets[i].workers.length;j++) {
                    if(sockets[i].workers[j].taskId == data.taskId) {
                        console.log('send failure');
                        sockets[i].emit('taskFailure' + data.taskId, data);
                        return;
                    }
                }
            }
        }
    }
};

module.exports = worker;