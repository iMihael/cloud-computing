var io,
    Worker = require('../models/worker'),
    Client = require('../models/client'),
    clientApi = require('./sock/client'),
    workerApi = require('./sock/worker'),
    sockets = [];

var socket = {
    getSockets: function(){
        return sockets;
    },
    getFreeWorker: function(){
        for(var i=0;i<sockets.length;i++) {
            if(sockets[i].modelType == 'worker' && sockets[i].model.status == 'ready') {
                return sockets[i];
            }
        }

        return false;
    },
    workerLogin: function(data, sock, success, failure){
        Worker.byIdAndSecret(data.id, data.secret, function(model){
            for(var i=0;i<sockets.length;i++) {
                if(sockets[i].modelType == 'worker' && sockets[i].model.id == model.id) {
                    //TODO: logout logged worker
                    console.error('worker already connect...');
                    failure(sock);
                    return;
                }
            }

            model.status = 'ready';
            sock.modelType = 'worker';
            sock.model = model;

            success(sock, data.localAddress);

        }, function(){
            console.error('worker not found!');
            failure(sock);
        });
    },
    clientLogin: function (data, sock, success, failure) {
        Client.byIdAndSecret(data.id, data.secret, function(model){

            for(var i=0;i<sockets.length;i++) {
                if(sockets[i].modelType == 'client' && sockets[i].model.id == model.id) {
                    //TODO: logout logged client
                    console.error('client already connect...');
                    failure(sock);
                    return;
                }
            }

            sock.modelType = 'client';
            sock.model = model;

            success(sock, data.localAddress);

        }, function(){
            console.error('Can not find client');
            failure(sock);
        });
    },
    disconnect: function(){
        if(this.modelType == 'worker') {
            this.model.goOffline();
        } else if(this.modelType == 'client') {
            //abort all workers!
            //send to worker event to abort all workout.
            if(this.hasOwnProperty('workers') && this.workers.length > 0) {
                for(var i=0;i<this.workers.length;i++) {
                    this.workers[i].model.status = 'ready';
                    this.workers[i].model.save();
                    //send abort signal
                    this.workers[i].emit('abortTask');
                }
            }
        }

        for(var i=0;i<sockets.length;i++) {
            if(sockets[i] == this) {
                sockets.splice(i, 1);
                return;
            }
        }
    },
    init: function(server){
        clientApi.init();
        workerApi.init();
        
        io = require('socket.io')(server);

        io.on('connection', function(sock){
            var query = sock.handshake.query;

            var success = function(sock, localAddress){
                sock.model.localAddress = localAddress;
                sock.model.save();

                sock.on('disconnect', socket.disconnect);

                sock.emit('logged');

                if(sock.modelType == 'worker') {
                    workerApi.addListeners(sock);
                } else if(sock.modelType == 'client') {
                    clientApi.addListeners(sock);
                }

                sockets.push(sock);
            };

            var failure = function(sock){
                sock.disconnect();
            };

            if(query.hasOwnProperty('type') && query.hasOwnProperty('id') && query.hasOwnProperty('secret') && query.hasOwnProperty('localAddress')) {
                if(query.type == 'worker') {
                    socket.workerLogin(query, sock, success, failure);
                } else if(query.type == 'client') {
                    socket.clientLogin(query, sock, success, failure);
                }
            }
        });
    }
};

module.exports = socket;