'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.addColumn('worker', 'localAddress', Sequelize.STRING, {
      allowNull: true
    });
    queryInterface.addColumn('worker', 'mappedAddress', Sequelize.STRING, {
      allowNull: true
    });
    queryInterface.addColumn('worker', 'relayedAddress', Sequelize.STRING, {
      allowNull: true
    });

    queryInterface.addColumn('client', 'localAddress', Sequelize.STRING, {
      allowNull: true
    });
    queryInterface.addColumn('client', 'mappedAddress', Sequelize.STRING, {
      allowNull: true
    });
    queryInterface.addColumn('client', 'relayedAddress', Sequelize.STRING, {
      allowNull: true
    });
    
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.removeColumn('worker', 'localAddress');
    queryInterface.removeColumn('worker', 'mappedAddress');
    queryInterface.removeColumn('worker', 'relayedAddress');

    queryInterface.removeColumn('client', 'localAddress');
    queryInterface.removeColumn('client', 'mappedAddress');
    queryInterface.removeColumn('client', 'relayedAddress');
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
