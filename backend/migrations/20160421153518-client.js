'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.createTable('client', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      secret: {
        type: Sequelize.STRING,
        allowNull: false
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {model: 'user', key: 'id'},
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      },
      successTasks: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      failedTasks: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      workTime: {
        type: Sequelize.DOUBLE,
        allowNull: false,
        defaultValue: 0
      }
    });


    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
  },

  down: function (queryInterface, Sequelize) {

    queryInterface.dropTable('client');
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
