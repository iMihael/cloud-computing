var Sequelize = require('sequelize');
var s = require('../components/db');

var User = s.define('user', {
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isEmail: true
        }
    },
    firstName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    lastName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    paranoid: true,
    tableName: 'user'
});

User.sync();

module.exports = User;