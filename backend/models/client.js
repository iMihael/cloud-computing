var Sequelize = require('sequelize'),
    s = require('../components/db'),
    User = require('./user');


var Client = s.define('client', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    secret: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {model: 'user', key: 'id'},
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        validate: {
            notEmpty: true
        }
    },
    successTasks: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    failedTasks: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    workTime: {
        type: Sequelize.DOUBLE,
        allowNull: false,
        defaultValue: 0
    },
    localAddress: {
        type: Sequelize.STRING,
        allowNull: true
    },
    mappedAddress: {
        type: Sequelize.STRING,
        allowNull: true
    },
    relayedAddress: {
        type: Sequelize.STRING,
        allowNull: true
    }
}, {
    paranoid: true,
    tableName: 'client',
    classMethods: {
        byUserId: function(id, success) {
            this.findAll({
                include: [
                    {
                        model: User,
                        as: 'user'
                    }
                ],
                where: {
                    userId: id
                }
            }).then(function(clients){
                success(clients);
            });
        },
        byIdAndUserId: function(id, userId, success, error) {
            this.findOne({
                include: [
                    {
                        model: User,
                        as: 'user'
                    }
                ],
                where: {
                    id: id,
                    userId: userId
                }
            }).then(function(client){
                if(client) {
                    success(client);
                } else {
                    error();
                }
            });
        },
        byIdAndSecret: function(id, secret, success, error){
            this.findOne({
                include: [
                    {
                        model: User,
                        as: 'user'
                    }
                ],
                where: {
                    id: id,
                    secret: secret
                }
            }).then(function(model){
                if(model) {
                    success(model);
                } else {
                    error();
                }
            });
        }
    }
});

Client.belongsTo(User, {foreignKey: 'userId'});

Client.sync();

module.exports = Client;