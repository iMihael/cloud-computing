var Sequelize = require('sequelize'),
    s = require('../components/db'),
    User = require('./user');


var Worker = s.define('worker', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    secret: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {model: 'user', key: 'id'},
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        validate: {
            notEmpty: true
        }
    },
    successTasks: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    failedTasks: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    workTime: {
        type: Sequelize.DOUBLE,
        allowNull: false,
        defaultValue: 0
    },
    localAddress: {
        type: Sequelize.STRING,
        allowNull: true
    },
    mappedAddress: {
        type: Sequelize.STRING,
        allowNull: true
    },
    relayedAddress: {
        type: Sequelize.STRING,
        allowNull: true
    },
    status: {
        type: Sequelize.ENUM('offline', 'ready', 'working'),
        allowNull: false,
        defaultValue: 'offline'
    }
}, {
    paranoid: true,
    tableName: 'worker',
    instanceMethods: {
        goOffline: function(){
            this.status = 'offline';
            this.save();
        }
    },
    classMethods: {
        readyWorkersCount: function(success){
            this.findAll({
                attributes: [[s.fn('COUNT', s.col('id')), 'count']],
                raw: true,
                where: {
                    status: 'ready'
                }
            }).then(success);
        },
        byUserId: function(id, success) {
            this.findAll({
                include: [
                    {
                        model: User,
                        as: 'user'
                    }
                ],
                where: {
                    userId: id
                }
            }).then(function(workers){
                success(workers);
            });
        },
        byIdAndSecret: function(id, secret, success, error){
            this.findOne({
                include: [
                    {
                        model: User,
                        as: 'user'
                    }
                ],
                where: {
                    id: id,
                    secret: secret
                }
            }).then(function(worker){
                if(worker) {
                    success(worker);
                } else {
                    error();
                }
            });
        },
        byIdAndUserId: function(id, userId, success, error) {
            this.findOne({
                include: [
                    {
                        model: User,
                        as: 'user'
                    }
                ],
                where: {
                    id: id,
                    userId: userId
                }
            }).then(function(worker){
                if(worker) {
                    success(worker);
                } else {
                    error();
                }
            });
        }
    }
});

Worker.belongsTo(User, {foreignKey: 'userId'});

Worker.sync();

module.exports = Worker;