var config = require('../config/config'),
    address = 'http://' + config.host + ':' + config.port,
    stun = require('./stun'),
    net = require('./net'),
    fs = require('fs'),
    file = require('./file'),
    randomstring = require("randomstring"),
    chunkSize = 512,
    sock;

var tasks = [];

var socket = {
    getTaskById: function(id) {
        for(var i=0;i<tasks.length;i++) {
            if(tasks[i].id == id) {
                return tasks[i];
            }
        }

        return false;
    },
    init: function(logged, failure){
        sock = require('socket.io-client').connect(
            address + "?type=client&id=" + config.id + "&secret=" + config.secret +
            "&localAddress=" + net.getLocalAddress()
        );

        sock.on('disconnect', function(){
            failure('socket disconnected');
        });

        sock.on('err', function(error){
            failure(error);
        });

        sock.on('logged', function(){
            logged();
        });

        return sock;
    },
    getFreeWorkersCount: function(success){
        sock.once('getFreeWorkersCount', success);
        sock.emit('getFreeWorkersCount');
    },
    createChannelToWorker: function(task, data, reader, success, failure){
        //TODO: implement TURN transport

        var workerAddress;

        if (data.hasOwnProperty('boolean') && data.boolean) {

            workerAddress = data.data;
            console.log('Trying to connect via local network...');
            net.localConnection(workerAddress.localAddress.address, workerAddress.localAddress.port, function (client) {
                console.log('Local connection success.');
                //start task!
                var writer = function(data){
                    client.write(data);
                };

                client.on('data', reader);
                success(writer);

            }, function () {

                console.log('Local connection failed.');
                console.log('Using STUN to communicate.');

                //make stun connection writer
                var writer = function(data){
                    task.udpSocket.send(data, 0, data.length, workerAddress.stunAddress.port, workerAddress.stunAddress.address);
                };

                task.udpSocket.on('message', reader);
                success(writer);
            });
        } else {
            failure(data);
        }
    },
    addTask: function(inputFilePath, inputCode, taskSuccess, taskFailure){
        var func = inputCode.toString();
        var match1 = func.match(/function \(([a-zA-Z_$][a-zA-Z0-9]*),\s*([a-zA-Z_$][a-zA-Z0-9]*)\){([\s\S]*)}/m);
        var match2 = func.match(/function \(([a-zA-Z_$][a-zA-Z0-9]*)\){([\s\S]*)}/m);

        if(!match1 && !match2) {
            taskFailure('wrong input code format');
            return;
        }
        
        if(inputFilePath && !fs.existsSync(inputFilePath)) {
            taskFailure('input file not found!');
            return false;
        }

        var taskId = config.id + randomstring.generate({
            length: 8,
            charset: 'alphabetic'
        });

        var writer;
        var task = {
            id: taskId
        };

        sock.once('taskFailure' + taskId, function(data){
            if(data && data.hasOwnProperty('reason')) {
                taskFailure(data.reason);
            } else {
                taskFailure(data);
            }
        });

        sock.once('addTask' + taskId, function (data) {


            socket.createChannelToWorker(task, data, function(bytes){

                //reader

                if(task.hasOwnProperty('outputFile') && task.outputFile.hasOwnProperty('size') && task.outputFile.receivedBytes < task.outputFile.size) {

                    fs.writeSync(task.outputFile.fd, bytes, 0, bytes.length);
                    task.outputFile.receivedBytes += bytes.length;

                    // console.log(task.outputFile.receivedBytes, bytes.length);

                    if(task.outputFile.receivedBytes >= task.outputFile.size) {
                        file.hashFile(task.outputFile.path, function(hash){

                            console.log('file received', hash);

                            if(hash == task.outputFile.hash) {
                                console.log('hash is right!');
                                socket.sendJson(writer, {
                                    method: 'fileReceived',
                                    data: {
                                        taskId: task.id
                                    }
                                });

                                sock.emit('finishTask', {
                                    taskId: task.id
                                });

                                task.taskSuccess(task.outputFile);

                            } else {
                                console.error('wrong hash!');
                            }
                        });
                    }

                } else {

                    //TODO: add tcp packet parser like in worker
                    var call = JSON.parse(bytes.toString());
                    if (call.hasOwnProperty('method') && call.hasOwnProperty('data') && socket.methods.hasOwnProperty(call.method)) {
                        socket.methods[call.method](writer, call.data, call.data.hasOwnProperty('taskId') ? socket.getTaskById(call.data.taskId) : null);
                    }
                }

            }, function(w){
                writer = w;

                task.writer = w;
                task.inputFilePath = inputFilePath;
                task.inputCode = inputCode.toString();
                task.taskSuccess = taskSuccess;
                task.taskFailure = taskFailure;

                tasks.push(task);

                socket.sendJson(writer, {
                    method: 'auth',
                    data: {
                        taskId: taskId
                    }
                });

            }, function(data){
                taskFailure(data);
            });

        });

        stun.connect(function(udpSocket, stunAddress){
            task.udpSocket = udpSocket;
            task.stunAddress = stunAddress;

            sock.emit('addTask', {
                id: taskId,
                stunAddress: stunAddress
            });
        }, taskFailure);


    },
    inputFile: function(task){
        var stats = fs.statSync(task.inputFilePath);
        task.inputFileSize = stats.size;
        file.hashFile(task.inputFilePath, function(hash){
            task.inputFileHash = hash;
            socket.sendJson(task.writer, {
                method: 'inputFile',
                data: {
                    taskId: task.id,
                    size: task.inputFileSize,
                    hash: hash
                }
            });
        });
    },
    methods: {
        ping: function(){
            console.log('ping received!');
        },
        auth: function(writer, data, task){
            //send input file
            if(data.status == 'success') {
                if(task.inputFilePath) {
                    socket.inputFile(task);
                } else {
                    socket.sendJson(writer, {
                        method: 'inputCode',
                        data: {
                            taskId: task.id,
                            inputCode: task.inputCode
                        }
                    });
                }
            } else {
                task.taskFailure('auth error');
            }
        },
        inputFile: function(writer, data, task){
            task.inputFileStream = fs.openSync(task.inputFilePath, 'r');
            task.inputFileSent = 0;
            console.log('sending input file...');
            do {
                var buf = new Buffer(chunkSize);
                var read = fs.readSync(task.inputFileStream, buf, 0, chunkSize, task.inputFileSent);

                if(read < chunkSize) {
                    buf = buf.slice(0, read);
                }

                task.inputFileSent += read;
                writer(buf);
            } while(task.inputFileSent < task.inputFileSize);

            console.log('done.');
        },
        fileReceived: function(writer, data, task){
            socket.sendJson(writer, {
                method: 'inputCode',
                data: {
                    taskId: task.id,
                    inputCode: task.inputCode
                }
            });
        },
        outputFile: function(writer, data, task) {
            task.outputFile = {
                size: data.size,
                receivedBytes: 0,
                hash: data.hash,
                name: randomstring.generate({
                    length: 8,
                    charset: 'alphabetic'
                })
            };

            task.outputFile.path = __dirname +  '/../storage/' + task.outputFile.name;
            task.outputFile.fd = fs.openSync(task.outputFile.path, "w+");

            socket.sendJson(writer, {
                method: 'outputFile',
                data: {
                    taskId: task.id,
                    status: 'ready'
                }
            });
        }
    },
    sendJson: function(writer, data){
        writer(new Buffer(JSON.stringify(data)));
    }
};

module.exports = socket;