var net = require('net'),
    ip = require('ip');

module.exports = {
    localConnection: function(address, port, success, error) {
        var client = new net.Socket();
        client.on('error', function(data){
            if(data.code == 'ECONNREFUSED') {
                error();
            }
        });

        client.connect(port, address, function(){
            success(client);
        });
    },
    getLocalAddress: function(){
        return ip.address();
    }
};