var socket = require('./components/socket'),
    fs = require('fs');

socket.init(function(){
    socket.getFreeWorkersCount(function(count){
        
        
        if(count >= 2) {

            var outputFiles = [];
            var successCount = 2;
            var success = function(oFile){
                outputFiles.push(oFile);
                successCount--;
                if(successCount == 0) {
                    console.log('tasks finished!');

                    var finalFd = fs.openSync(__dirname + '/storage/primes.txt', 'w+');
                    fs.closeSync(finalFd);

                    var finalFile = fs.createWriteStream(__dirname + '/storage/primes.txt');

                    for(var i=0;i<outputFiles.length;i++) {
                        fs.createReadStream(outputFiles[i].path).pipe(finalFile);
                        fs.unlinkSync(outputFiles[i].path);
                    }
                    
                }
            };
            
            socket.addTask(false, function(outputFile){

                var fs = require('fs');

                var start = 3;
                var stop = 10000;
                var outputFd = fs.openSync(outputFile, 'r+');

                nextPrime:
                    for(var i = start; i< stop;i++) {
                        for(var j = 2;j<i;j++) {
                            if(i % j == 0) {
                                continue nextPrime;
                            }
                        }

                        var buf = new Buffer(j + "\n");
                        fs.writeSync(outputFd, buf, 0, buf.length);
                    }

            }, success, function(error){
                console.log(error);
            });

            socket.addTask(false, function(outputFile){

                var fs = require('fs');

                var start = 10000;
                var stop = 20000;
                var outputFd = fs.openSync(outputFile, 'r+');

                nextPrime:
                    for(var i = start; i< stop;i++) {
                        for(var j = 2;j<i;j++) {
                            if(i % j == 0) {
                                continue nextPrime;
                            }
                        }

                        var buf = new Buffer(j + "\n");
                        fs.writeSync(outputFd, buf, 0, buf.length);
                    }

            }, success, function(error){
                console.log(error);
            });
        }
    });
}, function(error){
    console.log(error);
});
