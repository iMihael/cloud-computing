var socket = require('./components/socket'),
    fs = require('fs');

var storagePath =  __dirname + '/storage/';
var numbersPath = storagePath + 'numbers.txt';
var start = Date.now();

var readLine = function (file) {
    //file must by an object with fields fd and cursor
    var char;
    var line = "";
    var read = 0;
    do {
        var buf = new Buffer(1);
        read = fs.readSync(file.fd, buf, 0, 1, file.cursor);
        if (read > 0) {
            file.cursor++;
            char = buf.toString();
            line += char;
        }
    } while (char != "\n" && read > 0);

    if (read == 0) {
        return false;
    } else {
        return line.trim();
    }
};

var getLinesCount = function(file){
    var tLines = 0;
    var tCursor = file.cursor;
    while(readLine(file)) {
        tLines++;
    }
    file.cursor = tCursor;
    return tLines;
};

var minimumValueOfObject = function (values) {
    var keys = Object.keys(values);
    var minKey = keys[0];
    for (var i in values) {
        if (values[i] <= values[minKey]) {
            minKey = i;
        }
    }

    return minKey;
};

var file = {
    fd: fs.openSync(numbersPath, 'r+'),
    cursor: 0
};

var lines = getLinesCount(file);




socket.init(function(){
    socket.getFreeWorkersCount(function(count){
        console.log('workers:', count);
        var workers = [];

        if(count >= 2) {
            var workerLines = Math.ceil(lines / count);
            //create temp files
            for(var i=0;i<count;i++) {
                var worker = {};
                worker.tempFilePath = storagePath + 'temp' + i.toString();
                worker.tempFileFd = fs.openSync(worker.tempFilePath, 'w+');

                for(var j=0;j<workerLines;j++) {
                    var line = readLine(file);
                    
                    if(typeof line == 'string') {
                        var buf = new Buffer(line + "\n");
                        fs.writeSync(worker.tempFileFd, buf, 0, buf.length);
                    } else {
                        break;
                    }
                }

                fs.closeSync(worker.tempFileFd);
                workers.push(worker);
            }


            var doneCount = count;
            var sortedFiles = [];

            var merge = function(){

                var counter = lines;
                var values = {};

                var finalFd = fs.openSync(storagePath + 'parallelOutput.txt', 'w+');

                var checkLines = 0;

                for(var i=0;i<sortedFiles.length;i++) {
                    checkLines += getLinesCount(sortedFiles[i]);
                }

                if(checkLines == lines) {

                    do {
                        for (var i = 0; i < count; i++) {
                            if (values.hasOwnProperty(i)) {
                                continue;
                            }

                            var line = readLine(sortedFiles[i]);
                            if(typeof line == 'string') {
                                values[i] = parseInt(line);
                            }
                        }

                        var minKey = minimumValueOfObject(values);
                        var buf = new Buffer(values[minKey] + "\n");
                        fs.writeSync(finalFd, buf, 0, buf.length);
                        delete values[minKey];
                        counter--;
                    } while (counter > 0);

                    //delete temp files
                    for (var i = 0; i < workers.length; i++) {
                        fs.unlinkSync(workers[i].tempFilePath);
                    }

                    for (var i = 0; i < sortedFiles.length; i++) {
                        fs.unlinkSync(sortedFiles[i].path);
                    }

                    console.log(Date.now() - start);

                } else {
                    console.error('lines mismatch', checkLines, lines);
                }

                process.exit();

            };


            var workerDone = function(file){
                file.cursor = 0;
                sortedFiles.push(file);


                doneCount--;
                if(doneCount == 0) {
                    merge();
                }
            };
            var workerFailure = function(error){
                console.log(error);
                doneCount--;
                if(doneCount == 0) {
                    process.exit(1);
                }
            };


            for(var i=0;i<count;i++) {
                socket.addTask(workers[i].tempFilePath, function(inputPath, outputPath){

                    var fs = require('fs');
                    var inputFd = fs.openSync(inputPath, 'r+');
                    var outputFd = fs.openSync(outputPath, 'w+');

                    var readLine = function (file) {
                        //file must by an object with fields fd and cursor
                        var char;
                        var line = "";
                        var read = 0;
                        do {
                            var buf = new Buffer(1);
                            read = fs.readSync(file.fd, buf, 0, 1, file.cursor);
                            if (read > 0) {
                                file.cursor++;
                                char = buf.toString();
                                line += char;
                            }
                        } while (char != "\n" && read > 0);

                        if (read == 0) {
                            return false;
                        } else {
                            return line.trim();
                        }
                    };

                    var getLineByIndex = function (fd, index, offset) {
                        var temp = {
                            fd: fd,
                            cursor: offset ? offset : 0
                        };

                        for (var i = 0; i < index; i++) {
                            readLine(temp);
                        }

                        return readLine(temp);
                    };

                    var getLinesCount = function(file){
                        var tLines = 0;
                        var tCursor = file.cursor;
                        while(readLine(file)) {
                            tLines++;
                        }
                        file.cursor = tCursor;
                        return tLines;
                    };

                    var minimumValueOfObject = function (values) {
                        var keys = Object.keys(values);
                        var minKey = keys[0];
                        for (var i in values) {
                            if (values[i] <= values[minKey]) {
                                minKey = i;
                            }
                        }

                        return minKey;
                    };

                    var arrayElements = 100;

                    var file = {
                        fd: inputFd,
                        cursor: 0
                    };

                    var lines = getLinesCount(file);

                    var cycles = Math.ceil(lines / arrayElements);

                    var mergeLine = {};

                    var offsets = {
                        0: 0
                    };

                    for (var i = 0; i < cycles; i++) {
                        mergeLine[i] = 0;

                        offsets[i + 1] = offsets[i];

                        var array = [];
                        for (var j = 0; j < arrayElements; j++) {

                            var line = readLine(file);
                            if (typeof line == 'string') {
                                array.push(parseInt(line));
                            } else {
                                break;
                            }
                        }

                        array.sort(function (a, b) {
                            return a - b;
                        });

                        for (var k = 0; k < array.length; k++) {
                            var buf = new Buffer(array[k] + "\n");
                            fs.writeSync(outputFd, buf, 0, buf.length);
                            offsets[i + 1] += buf.length;
                        }
                    }

                    fs.ftruncateSync(inputFd, 0);

                    var counter = lines;
                    var values = {};

                    do {
                        for (var i = 0; i < cycles; i++) {
                            if (values.hasOwnProperty(i)) {
                                continue;
                            }


                            if (mergeLine[i] < arrayElements) {
                                var val = parseInt(getLineByIndex(outputFd, mergeLine[i], offsets[i]));
                                if (val) {
                                    values[i] = val;
                                }
                            }
                        }

                        var minKey = minimumValueOfObject(values);
                        var buf = new Buffer(values[minKey] + "\n");
                        fs.writeSync(inputFd, buf, 0, buf.length);
                        delete values[minKey];
                        mergeLine[minKey]++;
                        counter--;
                    } while (counter > 0);

                    var t = outputPath;
                    outputPath = inputPath;
                    inputPath = t;

                }, workerDone, workerFailure);
            }



        }
    });
}, function(data){
    console.error(data);
});