var fs = require('fs');

//https://en.wikipedia.org/wiki/External_sorting
//https://en.wikipedia.org/wiki/Merge_algorithm

var start = Date.now();

var arrayElements = 100;

var path = __dirname + '/storage/numbers.txt';

var outputFd = fs.openSync(__dirname + '/storage/numbers.sorted.txt', 'w+');
var tempPath = __dirname + '/storage/temp.txt';
var tempFd = fs.openSync(tempPath, 'w+');

var readLine = function (file) {
    //file must by an object with fields fd and cursor
    var char;
    var line = "";
    var read = 0;
    do {
        var buf = new Buffer(1);
        read = fs.readSync(file.fd, buf, 0, 1, file.cursor);
        if (read > 0) {
            file.cursor++;
            char = buf.toString();
            line += char;
        }
    } while (char != "\n" && read > 0);

    if (read == 0) {
        return false;
    } else {
        return line.trim();
    }
};

var getLineByIndex = function (fd, index, offset) {
    var temp = {
        fd: fd,
        cursor: offset ? offset : 0
    };

    for (var i = 0; i < index; i++) {
        readLine(temp);
    }

    return readLine(temp);
};

var getLinesCount = function(file){
    var tLines = 0;
    var tCursor = file.cursor;
    while(readLine(file)) {
        tLines++;
    }
    file.cursor = tCursor;
    return tLines;
};

var file = {
    fd: fs.openSync(path, 'r+'),
    cursor: 0
};

var lines = getLinesCount(file);
var cycles = Math.ceil(lines / arrayElements);

var mergeLine = {};
var offsets = {
    0: 0
};

for (var i = 0; i < cycles; i++) {
    mergeLine[i] = 0;

    offsets[i + 1] = offsets[i];

    var array = [];
    for (var j = 0; j < arrayElements; j++) {

        var line = readLine(file);
        if (typeof line == 'string') {
            array.push(parseInt(line));
        } else {
            break;
        }
    }

    array.sort(function (a, b) {
        return a - b;
    });

    for (var k = 0; k < array.length; k++) {
        var buf = new Buffer(array[k] + "\n");
        fs.writeSync(tempFd, buf, 0, buf.length);
        offsets[i + 1] += buf.length;
    }

    //console.log(i, cycles);
}

//time to merge

var minimumValueOfObject = function (values) {
    var keys = Object.keys(values);
    var minKey = keys[0];
    for (var i in values) {
        if (values[i] <= values[minKey]) {
            minKey = i;
        }
    }

    return minKey;
};

var counter = lines;
var values = {};

do {
    for (var i = 0; i < cycles; i++) {
        if (values.hasOwnProperty(i)) {
            continue;
        }

        // var lineNo = i * arrayElements + mergeLine[i];
        // values[i] = parseInt(getLineByIndex(tempFd, lineNo));

        if (mergeLine[i] < arrayElements) {
            var val = parseInt(getLineByIndex(tempFd, mergeLine[i], offsets[i]));
            if (val) {
                values[i] = val;
            }
        }
    }

    var minKey = minimumValueOfObject(values);
    var buf = new Buffer(values[minKey] + "\n");
    fs.writeSync(outputFd, buf, 0, buf.length);
    delete values[minKey];
    mergeLine[minKey]++;
    counter--;
    //console.log(counter);

} while (counter > 0);

fs.closeSync(tempFd);
fs.unlinkSync(tempPath);
fs.closeSync(outputFd);

var time = Date.now() - start;
console.log(time);





