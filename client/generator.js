var getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

var n = 20000;

if(process.argv.length > 2) {
    n = parseInt(process.argv[2]);
}

var fileName = __dirname + '/storage/numbers.txt';
var fs = require('fs');
var fd = fs.openSync(fileName, 'w+');

for(var i=0;i<n;i++) {
    var buf = new Buffer(getRandomInt(0, 2147483646) + '\n');
    fs.writeSync(fd, buf, 0, buf.length);
}

fs.closeSync(fd);
