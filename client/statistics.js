var proc = require('child_process');

var execProc = function(cmd) {
    var ret = proc.execSync(cmd);
    var buf = new Buffer(ret);
    var str = buf.toString().split('\n');
    if(str.length > 1) {
        return parseInt(str[str.length - 2]);
    } else {
        return 0;
    }
};

var parallelSort = 'node ./parallelSorting.js';
var sort = 'node ./sorting.js';
var generator = 'node ./generator.js';

var testNumbers = [
    5000,
    10000,
    25000,
    50000,
    75000,
    100000
];

for(var i=0;i<testNumbers.length;i++) {
    execProc(generator + ' ' + testNumbers[i].toString());

    var sortTime = execProc(sort);
    var parallelTime = execProc(parallelSort);

    console.log(testNumbers[i], sortTime, parallelTime);
}


